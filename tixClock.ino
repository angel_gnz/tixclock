#include <Adafruit_NeoPixel.h>
#include <Wire.h>
#include "RTClib.h"

#define PIN 3   // Pin que saca señal digital para NeoPixel

#define NUMPIXELS      27 // Numero de NeoPixels

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);
RTC_DS3231 rtc;

int delayT = 6000; // Tiempo entre cambio de patron
int hora = 0;
int hora1 = 2; // Primer número de la hora
int hora2 = 4; // Segundo número de la hora

int minuto = 0;
int minuto1 = 4; // Primer número del minuto
int minuto2 = 7; // Segundo número del minuto

int listaR[] = {0, 1, 2, 3, 4, 5, 6, 7, 8};

int rojo[] = {0, 17, 18};
int verde[] = {1, 2, 3, 14, 15, 16, 19, 20, 21};
int azul[] = {4, 5, 12, 13, 22, 23};
int rojo2[] = {6, 7, 8, 9, 10, 11, 24, 25, 26};

void setup() {
  pixels.begin(); // Inicializa libreria NeoPixel.
  randomSeed(analogRead(0));  //Seed aleatoria en conexión A0
  Serial.begin(9600);
  delay(3000);
  if (! rtc.begin()) {
    Serial.println("No hay un módulo RTC");
    while (1);
  }
  // Ponemos en hora, solo la primera vez, luego comentar y volver a cargar.
  // Ponemos en hora con los valores de la fecha y la hora en que el sketch ha sido compilado.
  //rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));

  // Reproduce secuencia de inicio
  intro();
}

int getTiempo(void) {
  DateTime now = rtc.now();
  hora = now.hour();
  minuto = now.minute();

  if (hora >= 10) {
    hora2 = hora % 10;
    hora1 = (hora / 10) % 10;
  } else
  {
    hora1 = 0;
    hora2 = hora;
  }
  if (minuto >= 10) {
    minuto2 = minuto % 10;
    minuto1 = (minuto / 10) % 10;
  } else
  {
    minuto1 = 0;
    minuto2 = minuto;
  }
}

void loop() {

  getTiempo();

  for (int i = 0; i < NUMPIXELS; i++) {

    pixels.setPixelColor(i, pixels.Color(0, 0, 0)); // Asigna color negro a todos los NeoPixels.

  }
  pixels.show(); // Con los NeoPixel en negro al hacer show los apaga.

  // Primer barra de horas
  randList(listaR, 3);

  for (int i = 0; i < hora1; i++)
  {
    pixels.setPixelColor(rojo[listaR[i]], pixels.Color(255, 0, 0)); // Pinta pixeles de rojo en la primer columna de horas
  }
  ordena(listaR, 9);

  // Segunda barra de horas

  randList(listaR, 9);

  for (int i = 0; i < hora2; i++)
  {
    pixels.setPixelColor(verde[listaR[i]], pixels.Color(0, 255, 0)); // Pinta pixeles de verde en la segunda columna de horas
  }
  ordena(listaR, 9);

  // Primer barra de minutos

  randList(listaR, 6);
  for (int i = 0; i < minuto1; i++)
  {
    pixels.setPixelColor(azul[listaR[i]], pixels.Color(0, 0, 255)); // Pinta pixeles de azul en la primer columna de minutos
  }
  ordena(listaR, 9);
  // Segunda barra de minutos

  randList(listaR, 9);
  for (int i = 0; i < minuto2; i++)
  {
    pixels.setPixelColor(rojo2[listaR[i]], pixels.Color(255, 0, 0)); // Pinta pixeles de rojo en la segunda columna de minutos
  }
  ordena(listaR, 9);

  pixels.show();
  delay(delayT);
}

// Metodo para ordenar lista
void ordena(int lista[], int tamano) {
  for (int i = 0; i < (tamano - 1); i++) {
    for (int o = 0; o < (tamano - (i + 1)); o++) {
      if (lista[o] > lista[o + 1]) {
        int t = lista[o];
        lista[o] = lista[o + 1];
        lista[o + 1] = t;
      }
    }
  }
}

// Metodo para desordenar lista
void randList(int lista[], int tamano) {
  for (int i = 0; i < tamano; i++) {
    int r = random(i, tamano);
    int temp = lista[i];
    lista[i] = lista[r];
    lista[r] = temp;
  }
}

// Animación de inicio
void intro(){
  for (int i = 0; i < 9; i++) {
    pixels.setPixelColor(i - 4, pixels.Color(0, 0, 0));
    pixels.setPixelColor(i - 3, pixels.Color(50, 0, 0));
    pixels.setPixelColor(i - 2, pixels.Color(100, 0, 0));
    pixels.setPixelColor(i - 1, pixels.Color(150, 0, 0));
    pixels.setPixelColor(i    , pixels.Color(255, 0, 0));

    pixels.setPixelColor((i + 9) - 4, pixels.Color(0, 0, 0));
    pixels.setPixelColor((i + 9) - 3, pixels.Color(0, 50, 0));
    pixels.setPixelColor((i + 9) - 2, pixels.Color(0, 100, 0));
    pixels.setPixelColor((i + 9) - 1, pixels.Color(0, 150, 0));
    pixels.setPixelColor((i + 9)    , pixels.Color(0, 255, 0));

    pixels.setPixelColor((i + 18) - 4, pixels.Color(0, 0, 0));
    pixels.setPixelColor((i + 18) - 3, pixels.Color(0, 0, 50));
    pixels.setPixelColor((i + 18) - 2, pixels.Color(0, 0, 100));
    pixels.setPixelColor((i + 18) - 1, pixels.Color(0, 0, 150));
    pixels.setPixelColor((i + 18)    , pixels.Color(0, 0, 255));

    if ( i <= 4) {
      pixels.setPixelColor(6, pixels.Color(0, 0, 0));
      pixels.setPixelColor(7, pixels.Color(0, 0, 0));
      pixels.setPixelColor(8, pixels.Color(0, 0, 0));

      pixels.setPixelColor(15, pixels.Color(0, 0, 0));
      pixels.setPixelColor(16, pixels.Color(0, 0, 0));
      pixels.setPixelColor(17, pixels.Color(0, 0, 0));

    }
    pixels.show();
    delay(90);
  }

  for (int i = 0; i < 9; i++) {
    pixels.setPixelColor(i - 4, pixels.Color(0, 0, 0));
    pixels.setPixelColor(i - 3, pixels.Color(0, 0, 50));
    pixels.setPixelColor(i - 2, pixels.Color(0, 0, 100));
    pixels.setPixelColor(i - 1, pixels.Color(0, 0, 150));
    pixels.setPixelColor(i    , pixels.Color(0, 0, 255));


    pixels.setPixelColor((i + 9) - 4, pixels.Color(0, 0, 0));
    pixels.setPixelColor((i + 9) - 3, pixels.Color(50, 0, 0));
    pixels.setPixelColor((i + 9) - 2, pixels.Color(100, 0, 0));
    pixels.setPixelColor((i + 9) - 1, pixels.Color(150, 0, 0));
    pixels.setPixelColor((i + 9)    , pixels.Color(255, 0, 0));

    pixels.setPixelColor((i + 18) - 4, pixels.Color(0, 0, 0));
    pixels.setPixelColor((i + 18) - 3, pixels.Color(0, 50, 0));
    pixels.setPixelColor((i + 18) - 2, pixels.Color(0, 100, 0));
    pixels.setPixelColor((i + 18) - 1, pixels.Color(0, 150, 0));
    pixels.setPixelColor((i + 18)    , pixels.Color(0, 255, 0));

    if ( i <= 4) {
      pixels.setPixelColor(23, pixels.Color(0, 0, 0));
      pixels.setPixelColor(24, pixels.Color(0, 0, 0));
      pixels.setPixelColor(25, pixels.Color(0, 0, 0));
      pixels.setPixelColor(26, pixels.Color(0, 0, 0));
    }
    pixels.show();
    delay(90);
  }

  for (int i = 0; i < 9; i++) {
    pixels.setPixelColor(i - 4, pixels.Color(0, 0, 0));
    pixels.setPixelColor(i - 3, pixels.Color(0, 50, 0));
    pixels.setPixelColor(i - 2, pixels.Color(0, 100, 0));
    pixels.setPixelColor(i - 1, pixels.Color(0, 150, 0));
    pixels.setPixelColor(i    , pixels.Color(0, 255, 0));

    pixels.setPixelColor((i + 9) - 4, pixels.Color(0, 0, 0));
    pixels.setPixelColor((i + 9) - 3, pixels.Color(0, 0, 50));
    pixels.setPixelColor((i + 9) - 2, pixels.Color(0, 0, 100));
    pixels.setPixelColor((i + 9) - 1, pixels.Color(0, 0, 150));
    pixels.setPixelColor((i + 9)    , pixels.Color(0, 0, 255));

    pixels.setPixelColor((i + 18) - 4, pixels.Color(0, 0, 0));
    pixels.setPixelColor((i + 18) - 3, pixels.Color(50, 0, 0));
    pixels.setPixelColor((i + 18) - 2, pixels.Color(100, 0, 0));
    pixels.setPixelColor((i + 18) - 1, pixels.Color(150, 0, 0));
    pixels.setPixelColor((i + 18)    , pixels.Color(255, 0, 0));

    if ( i <= 4) {
      pixels.setPixelColor(23, pixels.Color(0, 0, 0));
      pixels.setPixelColor(24, pixels.Color(0, 0, 0));
      pixels.setPixelColor(25, pixels.Color(0, 0, 0));
      pixels.setPixelColor(26, pixels.Color(0, 0, 0));
    }
    pixels.show();
    delay(90);
  }
}
