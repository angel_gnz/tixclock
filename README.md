TixClock
=======================
![ArduinoUno](https://img.shields.io/badge/ArduinoUno-R3-blue.svg?style=for-the-badge)


**Reloj Tix (TixClock) desarrollado para ArduinoUno**

En TinkerCad esta adaptado para funcionar con un modulo WiFi ya que no tiene un modulo RTC.

Preview
----------------
Se cuenta con un preview funcionando en TinkerCad.

https://www.tinkercad.com/things/4lQzX6NVrNf

![TixClock Preview](https://angel.brominc.com/images/tixClock.png)

Gabinete
----------------
El case tambien se encuentra en TinkerCad.

https://www.tinkercad.com/things/1kZEJKjexPJ

![TixCase Preview](https://angel.brominc.com/images/tixCase.png)